terraform {
  backend "s3" {
    bucket = "abc-node-aws-jenkins-terraform"
    key    = "jenkins.terraform.tfstate"
    region = "eu-west-1"
  }
}
