terraform {
  backend "s3" {
    bucket = "abc-node-aws-jenkins-terraform"
    key = "abc-node-aws-jenkins-terraform.tfstate"
    region = "eu-west-1"
  }
}
